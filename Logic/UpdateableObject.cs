﻿using System;
using Bitz.Modules.Core.Foundation.Interfaces;

namespace Bitz.Modules.Core.Foundation.Logic
{
    public abstract class UpdateableObject : BasicObject, IUpdateable
    {
        private Boolean _HasRegisteredWithUpdateStore;
        protected TimeSpan _LastUpdate;
        protected Boolean _Sleeping;

        protected UpdateableObject()
        {
            RegisterWithUpdateService();
        }

        protected UpdateableObject(Boolean skipUpdateableRegister)
        {
            if (skipUpdateableRegister) return;
            RegisterWithUpdateService();
        }

        public Boolean Awake
        {
            get => _Sleeping;
            set
            {
                if (!value) SleepUpdate();
                else WakeUpdate();
            }
        }

        public virtual Boolean TryUpdate(TimeSpan currentInstanceTime)
        {
            if (IsDisposed()) return false;

            if (_LastUpdate == TimeSpan.Zero) _LastUpdate = currentInstanceTime;
            TimeSpan time = currentInstanceTime - _LastUpdate;
            _LastUpdate = currentInstanceTime;

            if (time <= TimeSpan.Zero) return false;

            Update(time);
            return true;
        }

        public void RegisterWithUpdateService()
        {
            Injector.GetSingleton<IGameLogicService>().RegisterIUpdateable(this);
            _HasRegisteredWithUpdateStore = true;
            _LastUpdate = TimeSpan.Zero;
        }

        public void UnRegisterWithUpdateService()
        {
            if (_HasRegisteredWithUpdateStore)
            {
                Injector.GetSingleton<IGameLogicService>().UnRegisterIUpdateable(this);
                _HasRegisteredWithUpdateStore = false;
            }
        }

        protected abstract void Update(TimeSpan timeSinceLastUpdate);

        public override void Dispose()
        {
            UnRegisterWithUpdateService();
            base.Dispose();
        }

        public void SleepUpdate()
        {
            if (!_Sleeping)
            {
                _Sleeping = true;
                UnRegisterWithUpdateService();
            }
        }

        public void WakeUpdate()
        {
            if (_Sleeping)
            {
                _Sleeping = false;
                RegisterWithUpdateService();
            }
        }
    }
}