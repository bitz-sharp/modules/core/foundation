﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bitz.Modules.Core.Foundation.Logic
{
    /// <inheritdoc />
    /// <summary>
    ///     The base object provides a standard locking interface along with debug type counting
    /// </summary>
    public class BasicObject : IDisposable
    {
        private List<IDisposable> _Children;
        private Boolean _Disposed;
        private BasicObject _Parent;

        public BasicObject Parent
        {
            get => _Parent;
            set
            {
                if (value == _Parent) return;
                _Parent?.RemoveChild(this);
                _Parent = value;
                _Parent?.AddChild(this);
            }
        }

        protected List<IDisposable> Children => _Children?.ToList();

        public virtual void Dispose()
        {
            Parent = null;
            _Disposed = true;

            if (_Children == null) return;

            foreach (IDisposable basicObject in _Children.ToList()) basicObject.Dispose();
            _Children.Clear();
        }

        public virtual Boolean IsDisposed()
        {
            return _Disposed;
        }

        protected virtual void AddChild(IDisposable child)
        {
            if (_Children == null) _Children = new List<IDisposable>();
            _Children.Add(child);
            if (child is BasicObject o) o.Parent = this;
        }

        protected virtual void RemoveChild(IDisposable child)
        {
            _Children?.Remove(child);
        }
    }
}