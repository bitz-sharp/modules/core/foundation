﻿using System;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Foundation.Logic
{
    public interface IGameLogicService : IService
    {
        TimeSpan CurrentTickSize { get; }

        TimeSpan CurrentIntervalTime { get; }

        void RegisterIUpdateable(IUpdateable updateable);

        void UnRegisterIUpdateable(IUpdateable updateable);

        void TriggerUpdate(TimeSpan currentInstanceTime);

        /// <summary>
        ///     This method causes the GameLogicService to jump forward any time it may have gotten behind, Can be used to
        ///     stabilize the system after a blocking operation
        /// </summary>
        void ForceCatchup();
    }
}