﻿using System;

namespace Bitz.Modules.Core.Foundation.Debug.Logging
{
    public class LogEvent
    {
        public TimeSpan EventTime;
        public String Message;
        public LogSeverity Severity;
    }
}