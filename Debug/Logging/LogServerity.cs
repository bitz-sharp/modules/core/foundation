﻿using System;

namespace Bitz.Modules.Core.Foundation.Debug.Logging
{
    [Flags]
    public enum LogSeverity
    {
        VERBOSE = 1,
        DEBUG = 2,
        WARNING = 4,
        ERROR = 8,
        CRITICAL = 16,
        CUSTOM1 = 32,
        CUSTOM2 = 64,
        CUSTOM3 = 128
    }
}