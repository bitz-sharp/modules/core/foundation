﻿using System;
using System.Collections.Generic;

namespace Bitz.Modules.Core.Foundation.Debug.Logging
{
    public interface ILogConsumer : IDisposable, IInjectable
    {
        Boolean HasInit();
        void ConsumeLogEvent(LogEvent logEvent);
        void ConsumeLogEvent(List<LogEvent> logEvent);
    }
}