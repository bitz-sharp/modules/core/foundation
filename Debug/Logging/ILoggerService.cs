﻿using System;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Foundation.Debug.Logging
{
    public interface ILoggerService : IService
    {
        LogSeverity ActiveSeverityLevels { get; set; }
        void Log(LogSeverity Severity, String message);
        void RegisterLogConsumer(ILogConsumer logConsumer);
        void UnregisterLogConsumer(ILogConsumer logConsumer);
    }
}