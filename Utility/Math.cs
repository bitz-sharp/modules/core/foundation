﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Foundation.Utility
{
    public static class Math
    {
        public static Boolean Intersects(Vector4 a, Vector4 b)
        {
            return b.Z >= a.X && b.X <= a.Z && b.W >= a.Y && b.Y <= a.W;
        }
    }
}