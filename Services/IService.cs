﻿using System;
using Bitz.Modules.Core.Foundation.Interfaces;

namespace Bitz.Modules.Core.Foundation.Services
{
    public interface IService : IDisposable, IInjectable, INameable
    {
        Boolean DebugMode { get; set; }
        String GetServiceDebugStatus();
    }
}