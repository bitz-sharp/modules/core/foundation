﻿using System.Collections.Generic;
using System.Linq;

namespace Bitz.Modules.Core.Foundation.Services
{
    public static class ServiceManager
    {
        /// <summary>
        ///     A static lock preventing threading issues for affecting the creation or use of the singleton
        /// </summary>
        private static readonly List<IService> _ActiveServiceInstances = new List<IService>();

        public static IReadOnlyCollection<IService> ActiveServices => _ActiveServiceInstances.ToList();

        public static void ShutdownServices()
        {
            foreach (IService service in _ActiveServiceInstances.ToArray()) service.Dispose();
        }

        internal static void RegisterService(IService service)
        {
            _ActiveServiceInstances.Remove(service);
        }

        internal static void UnregisterService(IService service)
        {
            _ActiveServiceInstances.Remove(service);
        }
    }
}