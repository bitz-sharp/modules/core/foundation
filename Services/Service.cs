﻿using System;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Foundation.Services
{
    public abstract class Service : UpdateableObject, IService
    {
        private Boolean _DebugMode;

        protected Service(Boolean skipRegisterAsUpdateable = false) : base(skipRegisterAsUpdateable)
        {
            ServiceManager.RegisterService(this);
        }

        public Boolean DebugMode
        {
            get => _DebugMode;
            set => _DebugMode = value;
        }

        public String Name
        {
            get => GetType().Name;
            set => throw new NotSupportedException();
        }

        public override void Dispose()
        {
            Shutdown();
            ServiceManager.UnregisterService(this);
        }

        public abstract String GetServiceDebugStatus();

        /// <inheritdoc />
        public abstract void Initialize();

        protected abstract void Shutdown();
    }
}