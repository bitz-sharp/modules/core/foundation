﻿namespace Bitz.Modules.Core.Foundation
{
    public interface IInjectable
    {
        void Initialize();
    }
}