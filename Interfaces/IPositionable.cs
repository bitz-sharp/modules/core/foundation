﻿using OpenTK;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface IPositionable<T>
    {
        T Position { get; set; }
    }

    public interface IPositionable2D : IPositionable<Vector2>
    {
    }

    public interface IPositionable3D : IPositionable<Vector3>
    {
    }
}