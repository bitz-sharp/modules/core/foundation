﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface IRotateable<TX, TY>
    {
        TX Rotation { get; set; }
        TY RotationOrigin { get; set; }
    }

    public interface IRotateable2D : IRotateable<Single, Vector2>
    {
    }

    public interface IRotateable3D : IRotateable<Vector3, Vector3>
    {
    }
}