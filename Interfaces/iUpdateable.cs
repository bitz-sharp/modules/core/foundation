﻿using System;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface IUpdateable
    {
        Boolean TryUpdate(TimeSpan currentInstanceTime);
    }
}