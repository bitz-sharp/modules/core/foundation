﻿using System;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface IVisible
    {
        Boolean Visible { get; set; }
    }
}