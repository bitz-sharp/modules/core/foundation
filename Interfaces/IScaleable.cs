﻿using OpenTK;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface IScaleable<T>
    {
        T Scale { get; set; }
        T ScaleOrigin { get; set; }
    }

    public interface IScaleable2D : IScaleable<Vector2>
    {
    }

    public interface IScaleable3D : IScaleable<Vector3>
    {
    }
}