﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface ISizeable<T>
    {
        T Size { get; set; }
    }

    public interface ISizeable2D : ISizeable<Vector2>
    {
        Single Width { get; set; }
        Single Height { get; set; }
    }

    public interface ISizeable3D : ISizeable<Vector3>
    {
        Single Width { get; set; }
        Single Height { get; set; }
        Single Depth { get; set; }
    }
}