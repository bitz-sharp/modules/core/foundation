﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface IColourable
    {
        Vector4 Colour { get; set; }
        Single Alpha { get; set; }
    }
}