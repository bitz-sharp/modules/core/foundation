﻿using System;

namespace Bitz.Modules.Core.Foundation.Interfaces
{
    public interface INameable
    {
        String Name { get; set; }
    }
}