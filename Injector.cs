﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Bitz.Modules.Core.Foundation
{
    public static class Injector
    {
        private static readonly Dictionary<Type, RegistryEntry> _SingleMapRegistry = new Dictionary<Type, RegistryEntry>();
        private static readonly Dictionary<Type, List<RegistryEntry>> _MultiMapRegistry = new Dictionary<Type, List<RegistryEntry>>();
        private static SpinLock _Lock = new SpinLock();

        public static void RegisterMapping<T>(Type mappedType, Boolean singleton = false) where T : class, IInjectable
        {
            Lock();
            _SingleMapRegistry.Add(typeof(T), new RegistryEntry
            {
                ConstructionType = mappedType,
                Singleton = singleton,
                SingletonInstance = null
            });
            UnLock();
        }

        private static void Lock()
        {
            Boolean held = false;
            if (!_Lock.IsHeldByCurrentThread) _Lock.Enter(ref held);
        }

        private static void UnLock()
        {
            if (_Lock.IsHeldByCurrentThread) _Lock.Exit();
        }

        public static void RegisterMultiMapping<T>(Type mappedType, Boolean singleton = false) where T : class, IInjectable
        {
            Lock();
            if (!_MultiMapRegistry.ContainsKey(typeof(T))) _MultiMapRegistry.Add(typeof(T), new List<RegistryEntry>());
            _MultiMapRegistry[typeof(T)].Add(new RegistryEntry
            {
                ConstructionType = mappedType,
                Singleton = singleton,
                SingletonInstance = null
            });
            UnLock();
        }

        public static T GenerateInstance<T>() where T : class, IInjectable
        {
            Lock();
            RegistryEntry rEntry = _SingleMapRegistry.FirstOrDefault(a => a.Key == typeof(T)).Value;
            if (rEntry.ConstructionType == null)
            {
                UnLock();
                return null;
            }

            if (rEntry.Singleton)
            {
                UnLock();
                throw new BadInjectionException("Trying to generate an instance of a type marked as singleton, use GetSingleton instead");
            }

            IInjectable instance = (IInjectable) Activator.CreateInstance(rEntry.ConstructionType);
            instance.Initialize();
            UnLock();
            return (T) instance;
        }

        public static T GetSingleton<T>() where T : class, IInjectable
        {
            Lock();
            RegistryEntry rEntry = _SingleMapRegistry.FirstOrDefault(a => a.Key == typeof(T)).Value;
            if (rEntry.ConstructionType == null)
            {
                UnLock();
                return null;
            }

            if (rEntry.Singleton)
            {
                if (rEntry.SingletonInstance == null)
                {
                    rEntry.SingletonInstance = (IInjectable) Activator.CreateInstance(rEntry.ConstructionType);
                    rEntry.SingletonInstance.Initialize();
                }

                UnLock();
                return (T) rEntry.SingletonInstance;
            }

            UnLock();
            throw new BadInjectionException("Trying to get the singleton of a type registered as an instance generator, use GenerateInstance instead");
        }

        public static IEnumerable<T> GetMultiMap<T>() where T : class, IInjectable
        {
            Lock();
            KeyValuePair<Type, List<RegistryEntry>> mmEntry = _MultiMapRegistry.FirstOrDefault(a => a.Key == typeof(T));
            if (mmEntry.Value == null) yield break;
            foreach (RegistryEntry rEntry in mmEntry.Value)
            {
                if (rEntry.ConstructionType == null)
                {
                    UnLock();
                    yield return null;
                }

                if (rEntry.Singleton)
                {
                    if (rEntry.SingletonInstance == null)
                    {
                        rEntry.SingletonInstance = (IInjectable) Activator.CreateInstance(rEntry.ConstructionType ?? throw new InvalidOperationException());
                        rEntry.SingletonInstance.Initialize();
                    }

                    UnLock();
                    yield return (T) rEntry.SingletonInstance;
                    Lock();
                }
                else
                {
                    IInjectable instance = (IInjectable) Activator.CreateInstance(rEntry.ConstructionType ?? throw new InvalidOperationException());
                    instance.Initialize();
                    UnLock();
                    yield return (T) instance;
                    Lock();
                }
            }

            UnLock();
        }

        private class RegistryEntry
        {
            public Type ConstructionType;
            public Boolean Singleton;
            public IInjectable SingletonInstance;
        }
    }
}