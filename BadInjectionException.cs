﻿// // --------------------------------
// // -- File Created 	:  //
// // -- File Part of the ABitzProject Solution, project Web-Bridge.net
// // -- Edited By : 
// // --------------------------------

using System;

namespace Bitz.Modules.Core.Foundation
{
    public class BadInjectionException : Exception
    {
        public BadInjectionException(String msg) : base(msg)
        {
        }
    }
}