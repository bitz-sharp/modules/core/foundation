﻿using System;
using System.Threading;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Foundation
{
    public class Singleton<T> : BasicObject where T : class
    {
        private static Lazy<T> _Instance = new Lazy<T>(GenerateSingleton, LazyThreadSafetyMode.ExecutionAndPublication);

        /// <summary>
        ///     Public facing Getter for the lazy Instance
        /// </summary>
        public static T Instance => _Instance.Value;

        /// <summary>
        ///     Function call used by System.Lazy to generate the singleton
        /// </summary>
        /// <returns></returns>
        private static T GenerateSingleton()
        {
            T returnInstance = Activator.CreateInstance(typeof(T)) as T;

            System.Diagnostics.Debug.Assert(returnInstance != null, "returnInstance != null");

            return returnInstance;
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_Instance.Value == this) _Instance = new Lazy<T>(GenerateSingleton, LazyThreadSafetyMode.ExecutionAndPublication);
        }
    }
}