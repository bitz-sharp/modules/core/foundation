﻿using System;

namespace Bitz.Modules.Core.Data
{
    public interface IDataService
    {
        T GetValue<T>(String name);
        Boolean HasValue(String name);
        void SetValue<T>(String name, T value);
    }
}