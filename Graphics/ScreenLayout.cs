﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Foundation.Graphics
{
    public struct ScreenLayout
    {
        public readonly Vector2 Size;
        public readonly Single FPS;
        public readonly Boolean CanBeHorizontal;
        public readonly Boolean CanBeVertical;
        public readonly IntPtr OverrideDC;
        public readonly IntPtr OverrideHandle;


        public ScreenLayout(Vector2 size, Single fps = 60, Boolean canBeHorizontal = true, Boolean canBeVertical = false)
        {
            Size = size;
            FPS = fps;
            CanBeHorizontal = canBeHorizontal;
            CanBeVertical = canBeVertical;
            OverrideDC = IntPtr.Zero;
            OverrideHandle = IntPtr.Zero;
        }

        public ScreenLayout(Vector2 size, IntPtr overrideDC, IntPtr overrideHandle, Single fps = 60)
        {
            Size = size;
            FPS = fps;
            CanBeHorizontal = true;
            CanBeVertical = false;
            OverrideDC = overrideDC;
            OverrideHandle = overrideHandle;
        }
    }
}